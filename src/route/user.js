var express = require("express");
const bcrypt = require("bcrypt");
var pool = require("./../db/conn");

var jwt = require("jsonwebtoken");
require("dotenv").config();
var app = express();
var user_router = express.Router();

user_router.use(express.json());

// /user/registration/
//common password for all user :1234
user_router.post("/registration", (req, res) => {

    let f_nm = req.body.first_name;
    let l_nm = req.body.last_name;
    let c_nm = req.body.company_name;
  //  let city = req.body.city;
    let p_no = req.body.phone;
    let email = req.body.email;
    let rol_id = req.body.role_id;
    let i_active=req.body.is_active;
    let password = req.body.password;
    let cpassword = req.body.cpassword;
    console.log(req.body)
    if (cpassword == password) {

         let select_query = 'SELECT * FROM `admin` WHERE `email`=?';
         pool.query(select_query, [email], (err, result) => {
             if (result.length > 0) {
                 res.status(409).send({
                     StatusCode: res.statusCode,
                     message: "User already exists..."
                 });

             }
            else {
                let encryptpass = bcrypt.hashSync(password, 10)
                console.log(encryptpass)
                let query = `insert into admin(first_name, last_name,company_name,phone,email,role_id,password,is_active) VALUES(?,?,?,?,?,?,?,?)`;

                pool.query(query, [f_nm,l_nm,c_nm,p_no,email,rol_id,encryptpass,i_active], (err, rows) => {
                    if (err) throw err;
                    console.log("Row inserted with id = " + rows.insertId);
                    res.status(200).send({
                        StatusCode: res.statusCode,
                        message: "User Value accepted..."
                    });
                });
             }
         })

    } else {
        res.status(401).send({
            StatusCode: res.statusCode,
            message: "password and confirm password do not match"
        });

    }

})


//user login :user/login
user_router.post("/login", (req, res) => {
    let email = req.body.email;
    let password = req.body.password;
    let query = "SELECT * FROM `admin` WHERE email=?";

    pool.query(query, [email], (err, result) => {
        if (result.length < 1) {
            res.status(404).send({
                StatusCode: res.statusCode,
                message: "User Not Exist.."
            });
        }
        else {
            if (bcrypt.compareSync(password, result[0].password)) {
                //create token 1.payload,2.secret key,3.time
                const token = jwt.sign({ 
                    first_name: result[0].first_name,
                    phone: result[0].phone,
                    email: result[0].email
                }, 
                process.env.SECRET_KEY,//secret key
                {
                    expiresIn: "1 days"
                });
                

                res.status(200).send({
                    first_name: result[0].first_name,
                    phone: result[0].phone,
                    email: result[0].email,
                    company_name: result[0].company_name,
                    //: result[0].phone,
                    email: result[0].email,
                    token:token,
                    StatusCode: res.statusCode,
                    message: "Authenticated User...and Generate token.."
                });
            }
            else {
                res.status(403).send({
                    StatusCode: res.statusCode,
                    message: "Not Authenticated User ...Password matching fail.."
                });

            }

        }

    })
})

module.exports = user_router;



