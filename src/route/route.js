var express = require('express');
const req = require('express/lib/request');
const res = require('express/lib/response');
var pool = require('../db/conn'); //connection pool
const check_auth = require('../middleware/check_auth');
const  bcrypt = require('bcrypt');

var app = express();

var app_router = express.Router()
app_router.use(express.json());

//localhost:8001/api/user
//token allow for this path
app_router.get("/user",check_auth,(req, res) => {
    pool.query("select  COUNT(user_id) as total from admin", (err, result) => {
        console.log(err)
        if (err) {
            res.send(
                {
                    StatusCode: res.statusCode = 400,
                    message: "No Fetch Data... "
                }
            )

        }
        else {
            let count = result[0].total;//find total record
            if (0 < count) {
                pool.query("select first_name,last_name,city,phone_no from admin", (err, result2) => {
                    if (err) {
                        res.send(
                            {
                                StatusCode: res.statusCode = 400,
                                message: "Not Fetch User Data... "
                            }
                              )
                    }
                    else {
                        res.send(result2)
                    }
                })
            }
            else {
                res.send(
                    {
                        StatusCode: res.statusCode = 400,
                        message: "No user Record available..."
                    }
                )
            }
        }
    });
})
//login
//localhost:3000/api/user/login/
app_router.post("/user/login",(req, res) => {
    let email = req.body.email;
    let password = req.body.password;

    pool.query("SELECT * FROM `admin WHERE email=? AND password=?", [email, password],
        (err, result) => {
            if (err) {
                console.log(err)
            } else {
                if (result.length > 0) {

                    res.send(
                        {
                            StatusCode: res.statusCode = 200,
                            message: "Authentication User..."
                        }
                    )
                } else {
                    res.send({
                        StatusCode: res.statusCode = 400,
                        message: "no user found"
                    })
                }
            }
        });



})
//get one  user by id
//localhost:8001/api/user/2
app_router.get("/user/:id", (req, res) => {
    // console.log("Fetching user with id:", req.params.id)
    pool.query("select * from admin where user_id=?", [req.params.id], (err, result) => {
        if (err) {
            return console.log("", err);
        }
        else {
            if (result.length > 0) {
                console.log(result)
                res.send({
                    StatusCode: res.statusCode = 200,
                    message: "Specific User Found"
                })
            } else {
                res.send({
                    StatusCode: res.statusCode = 400,
                    message: "not match id.."
                })
            }
        }

    });
})

// add query for body-parser data
// router.post("/user/",(req,res)=>{
//     console.log(req.body)
// })



//add User Query fro json data
//localhost:3000/api/user 
app_router.post("/user", async (req, res) => {

    let f_nm = req.body.first_name;
    let l_nm = req.body.last_name;
    let city = req.body.city;
    let p_no = req.body.phone_no;
    let email = req.body.email_id;
    let rol_id = req.body.role_id;
    let password = req.body.password;
    let cpassword = req.body.cpassword;

   
        if (cpassword == password) {
            let query = `insert into admin(first_name, last_name,city, phone_no, email_id,rol_id, password) VALUES(?,?,?,?,?,?,?)`;
    
            pool.query(query, [f_nm, l_nm, city, p_no, email,rol_id, password], (err, rows) => {
                if (err) throw err;
                console.log("Row inserted with id = " + rows.insertId);
                res.send({
                    StatusCode: res.statusCode = 201,
                    message: "User Value accepted..."
                })
                
            });
        } else {
    
            res.send({
                StatusCode: res.statusCode = 400,
                message: "password and confirm password do not match"
            });
        }
   
    
});

//delete by user id  Query
//localhost:3000/api/user/2
app_router.delete("/user/:id", (req, res) => {

    let query = "delete from admin WHERE user_id = ?";
    let user_id = req.params.id;

    pool.query(query, user_id, (err, rows) => {
        if (err) throw err;

        console.log('Number of rows deleted = ' + rows.affectedRows);
        res.send({
            StatusCode: res.statusCode = 200,
            message: "User record Deleted...."
        });
    });
})
//delete by user email id  Query
app_router.delete("/user/:email", (req, res) => {

    let query = "delete from admin where email=?";
    let email = req.params.email;
    

    pool.query(query,email,(err, rows) => {
        if (err) throw err;

   //     res.send('Number of rows deleted = ' + rows.affectedRows)
        console.log('Number of rows deleted = ' + rows.affectedRows);
      //  console.log(email)
        res.send({
            StatusCode: res.statusCode = 200,
            message: "User record Deleted...."
        });
    });
})

//update user
app_router.put("/user/:id", (req, res) => {
    var fir_name = req.body.first_name;
    var las_name = req.body.last_name;
    var com_name = req.body.company_name;
    var phone_no = req.body.phone;
    var email_id = req.body.email;
    var password = req.body.password;
    console.log(req.body)
    // let query1 = `select * from admin where user_id=?`
    let query = `update admin set first_name=?,last_name=?,company_name=?,phone=?,email=?,password=? where user_id=?`;
    let user_id = req.params.id;
    let encryptpass = bcrypt.hashSync(password, 10)
    console.log(encryptpass)
    console.log(user_id)
    // pool.query(query1,[fir_name,las_name,com_name, phone_no, email_id,encryptpass,user_id], (err, result) => {
    //     if (err) throw err;
    //     console.log('affected..',result.affectedRows);
    //     res.send('Record Updated..');
    
    pool.query(query,[fir_name,las_name,com_name, phone_no, email_id,encryptpass,user_id], (err, result) => {
        if (err) throw err;
        console.log('affected..',result.affectedRows);
        res.send('Record Updated..');
       
    });
})

module.exports = {app_router,app};

// app_router.post("user/project",(req,res)=>{
//     let p_name = req.body.project_name;
//     let i_active = req.body.is_active;
//     console.log(req.body)
//     let query=`insert into(project_name,user_id,is_active) VALUES (?,?,?,?)`

//     pool.query(query, [p_name,i_active], (err, rows) => {
//         if (err) throw err;
//         console.log("Row inserted with id = " + rows.insertId);
//         res.status(200).send({
//             StatusCode: res.statusCode,
//             message: "project accepted..."
//         })
//     })
// })
