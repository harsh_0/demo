var {app_router,app}=require("./src/route/route");
var user_router=require("./src/route/user")
var proc_router=require("./src/route/proc")
const port = 8001 || process.env.PORT;

app.use('/api', app_router)
app.use('/user', user_router)
app.use('/project',proc_router)


//new line
//url for bad request it means enter url not found 
app.use((req,res,next)=>{
    res.status(404).json({
        massage:"Page not Found,URL not Found/ Bad URL"
    })
    next();
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})